﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour
{
    public Sprite layer_blue, layer_red;

    void OnMouseDown()
    {
        GetComponent<SpriteRenderer>().sprite = layer_blue;
    }
    void OnMouseUp()
    {
        GetComponent<SpriteRenderer>().sprite = layer_red;
    }

    void OnMouseUpAsButton()
    {
        switch (gameObject.name)
        { 
            case "Play" :
                Application.LoadLevel("Play");
                break;
        }
    }

}
