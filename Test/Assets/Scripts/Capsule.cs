﻿using UnityEngine;
using System.Collections;

public class Capsule : MonoBehaviour {

    public float speed, tild;
    private Vector3 target = new Vector3(0, 1.39f, 0);

    void Start()
    {

    }

    void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * speed);
        if (transform.position == target && target.y != 0.1f)
        {
            target.y = 0.1f;
        }
        else if (transform.position == target && target.y == 0.1f)
        {
            target.y = 1.39f;
        }

        transform.Rotate(Vector3.left, tild);
    }
}
